package com.neoexpert;

import java.util.*;
public class Cron{
	private Runnable runnable;
	public Cron(Runnable runnable){
		this.runnable=runnable;
		calcNextTime();
	}

	public Date getNextTime(){
		return calendar.getTime();	
	}

	private Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
	private void calcNextTime(){
		Date date = new Date();
		calendar.setTime(date);   
		int minute=calendar.get(Calendar.MINUTE);       
		int rm=10-minute%10;
		/*
		if(rm>5)
			minute+=rm-5;
		else
			minute+=rm+5;
		*/
		minute+=rm;
		calendar.set(Calendar.MINUTE,  minute);
		calendar.set(Calendar.SECOND,  0);
	}

	public long waitTillNextTime(){
		long toWait=calendar.getTimeInMillis()-System.currentTimeMillis();
		try{
			Thread.sleep(toWait);
			return toWait;
		}
		catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	public void run()throws Exception{
		while(true){

			synchronized(this){
				calcNextTime();
			}
			runnable.run();
		}
	}
}
