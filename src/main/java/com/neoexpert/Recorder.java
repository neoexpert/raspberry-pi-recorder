package com.neoexpert;

import java.util.*;
import java.text.SimpleDateFormat;
import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.lang.ProcessBuilder.*;
import java.lang.reflect.*;
import java.net.*;
import org.json.*;
import java.util.concurrent.atomic.AtomicLong;
import java.nio.file.*;


public class Recorder implements Runnable{
	Cron cron;
	private String root_dir_path="./video/";
	File root_dir;

	public void start()throws Exception{
		cron=new Cron(this);
		System.out.println(cron.getNextTime());
		root_dir=new File(root_dir_path);
		recreateManifests(root_dir);
		createIndex(root_dir);
		cron.run();
		//run();
	}

	public long waitAndTerm(){
		System.out.println(pid);
		long duration=cron.waitTillNextTime();
		{
			//kill -SIGINT %1|
			ProcessBuilder pb = new ProcessBuilder("/bin/kill", "-SIGINT", Integer.toString(pid));
			pb.redirectOutput(Redirect.INHERIT);
			pb.redirectError(Redirect.INHERIT);
			try{
				Process p = pb.start();
				pid=getPid(p);
				p.waitFor();
			}
			catch(Exception e)
			{
				e.printStackTrace(System.err);
			}
		}
		return duration;
	}
	private int pid;
	@Override
	public void run(){
		System.out.println();
		System.out.println("tick");
		String dirpath=root_dir_path+getDate();
		File dir=new File(dirpath);
		if(!dir.exists())
		{
			System.out.println("mkdir: ");
			System.out.println(dir.getAbsolutePath());
			dir.mkdir();
			createIndex(root_dir);
			try{
				copyFile(new File(root_dir,"vid.html"),new File(dir,"index.html"));
			}
			catch(Exception e){
				e.printStackTrace(System.err);
			}
		}
		checkDiskUsage(root_dir);

		String time=getTime();
		String imagepath=
		dirpath+"/"+time;
		//raspivid -o video.h264 -t 10000
		final AtomicLong duration=new AtomicLong();
		{
			ProcessBuilder pb = new ProcessBuilder("/usr/bin/raspivid","-n", "-fps", "4","-b","120000", "-w", "1024", "-h", "768", "-t", "6000000", "-o", imagepath+".h264");
			pb.redirectOutput(Redirect.INHERIT);
			pb.redirectError(Redirect.INHERIT);
			Process p=null;
			try{
				p = pb.start();
				pid=getPid(p);
			}
			catch(Exception e)
			{
				e.printStackTrace(System.err);
			}
			finally{
			
				duration.set(waitAndTerm());
				//if(p!=null)
					//p.waitFor();
			}
		}
		System.out.println("video taken");
		//MP4Box -add video.h264 -dash 1000 -rap -frag-rap 15_20.mp4
		{
			ProcessBuilder pb = new ProcessBuilder("/usr/bin/MP4Box", "-add", imagepath+".h264","-dash","1000","-rap","-frag-rap",  imagepath+".mp4");
			pb.redirectOutput(Redirect.INHERIT);
			pb.redirectError(Redirect.INHERIT);
			new Thread(new Runnable(){
				@Override
				public void run(){
					try{
						Process p = pb.start();
						p.waitFor();
						new File(imagepath+".h264").delete();
				writeManifest(imagepath, time+"_dashinit.mp4",getDuration(imagepath+"_dashinit.mp4"));
					}
					catch(Exception e)
					{
						e.printStackTrace(System.err);
					}
				}
			}).start();
		}
		System.out.println("next tick at:");
		System.out.println(cron.getNextTime());
	}
	private void writeManifest(String path, String filename, long duration){
		JSONObject jo=new JSONObject();
		jo.put("filename",filename);
		jo.put("duration",duration);
		try{
		FileOutputStream fos=new FileOutputStream(path+".json");
		String json=jo.toString();

		fos.write(json.getBytes());
		}
		catch(IOException e){
			e.printStackTrace(System.err);
		}
	}
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat stf = new SimpleDateFormat("HH_mm");

	public static String getDate() {
		Calendar cal = Calendar.getInstance();
		return sdf.format(cal.getTime());
	}
	private String getTime(){
		Calendar cal = Calendar.getInstance();
		int minute=cal.get(Calendar.MINUTE);       
		minute=(minute/10)*10;
		cal.set(Calendar.MINUTE,  minute);
		cal.set(Calendar.SECOND,  0);
		return stf.format(cal.getTime());
	}
	private static void copyFile(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			is.close();
			if(os!=null)
			os.close();
		}
	}

	private void recreateManifests(File dir){
		/*
		try{
			Files.list(dir.toPath())
				.forEach(e->{System.out.println(e);
					e.list().forEach(f->{System.out.println(f);});});

		}
		catch(IOException e){
			e.printStackTrace(System.err);
		}*/
	
	}
	private void createIndex(File dir){
		try{
			File[] dirs = dir.listFiles(new FileFilter() {
				@Override
				public boolean accept(File file) {
					return file.isDirectory();
				}
			});
			Arrays.sort(dirs, Collections.reverseOrder());
			StringBuilder sb=new StringBuilder();
			for(int i=0;i<dirs.length;i++){
				File d=dirs[i];
				String name=d.getName();
				if(name.length()<4)
					continue;
				sb
				.append("<a href=\"../")
				.append(name)
				.append("\">")
				.append(name)
				.append("</a>");
			}
			FileOutputStream fos=new FileOutputStream(new File(root_dir, "ajax_index.html"));
			fos.write(sb.toString().getBytes());
		}
		catch(IOException e){
			e.printStackTrace(System.err);
		}
	
	}
	public int getPid(Process p){
		if(p.getClass().getName().equals("java.lang.UNIXProcess")) {
			/* get the PID on unix/linux systems */
			try {
				Field f = p.getClass().getDeclaredField("pid");
				f.setAccessible(true);
				return f.getInt(p);
			}
			catch (Throwable e) {
				e.printStackTrace(System.err);
			}
		}  
		return -1;
	}


	private static final long ONEGB=1024*1024*1024;
	boolean deleteDirectory(File directoryToBeDeleted) {
		File[] allContents = directoryToBeDeleted.listFiles();
		if (allContents != null) {
			for (File file : allContents) {
				deleteDirectory(file);
			}
		}
		return directoryToBeDeleted.delete();
	}

	private void checkDiskUsage(File dir){
		//long totalSpace = file.getTotalSpace(); //total disk space in bytes.
		//long usableSpace = dir.getUsableSpace(); ///unallocated / free disk space in bytes.
		long freeSpace = dir.getFreeSpace(); //unallocated / free disk space in bytes.


		if(freeSpace<ONEGB){
			File[] dirs = dir.listFiles(new FileFilter() {
				@Override
				public boolean accept(File file) {
					if(file.isDirectory()){
						return file.getName().length()==10;
					}
					return false;
				}
			});
			if(dirs.length==0)return;
			Arrays.sort(dirs);
			System.out.print("dir to del:");
			System.out.print(dirs[0].getName());
			deleteDirectory(dirs[0]);
		}



	}
	public static ServerSocket ss;

	public static void main(String[] args)throws Exception{
		ss = null;

		try {
			ss = new ServerSocket(1044);
		} catch (IOException e) {
			System.err.println("Application already running!");
			System.exit(-1);
		}
		Recorder r=new Recorder();
		r.start();
	}

	private void recreateManifests(File dir){
		try{
		Files.list(dir.toPath())
			.filter(p -> Files.isDirectory(p))
			.filter(p -> p.getFileName().toString().length()==10)
			.forEach(p->{
				System.out.println(p);
				try{
				Files.list(p)
					.filter(f -> f.toString().endsWith(".mp4"))

					.forEach(f->{
						String name=f.getFileName().toString();
						System.out.println("filename: "+name);
						String path=f.toString();
						path=path.substring(0,path.lastIndexOf('_'));
						System.out.println("path: "+path);
						writeManifest(path,name,getDuration(f.toString()));
					});
				}
				catch(IOException e){
				e.printStackTrace(System.err);
				}
			});
		}
		catch(IOException e){
			e.printStackTrace(System.err);
		}
	}

	public long getDuration(String f){
		//ffprobe -i video/2019-08-17/21_00_dashinit.mp4 -show_entries format=duration -v quiet -of csv="p=0"
		ProcessBuilder pb = new ProcessBuilder("/bin/sh", "./length.sh", f);
		try{
			Process p = pb.start();
			InputStream fromError = p.getErrorStream();
			BufferedReader reader =
				new BufferedReader(new InputStreamReader(p.getInputStream()));


			String line=reader.readLine();
			if(line==null)
				return 0;
			float fdur=Float.parseFloat(line)*1000;
			int x;

			try {
				while((x = fromError.read()) != -1)
					System.err.print((char)x);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			p.waitFor();
			int exit = p.exitValue();
			System.out.println("Exited with code " + exit);
			return (long)fdur;
		}
		catch(Exception e)
		{
			e.printStackTrace(System.err);
		}
		return 0;
	}
}
