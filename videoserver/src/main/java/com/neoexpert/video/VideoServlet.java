package com.neoexpert.video;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import java.util.*;
import org.json.*;

public class VideoServlet extends HttpServlet{
	private static Object lock=new Object();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String cmd=req.getParameter("cmd");
		String path=req.getParameter("p");
		if(cmd!=null)
		switch(cmd){
			case "get_segment_count":

				File dir = new File("./root/",path);
				File[] dirs = dir.listFiles(new FileFilter() {
					@Override
					public boolean accept(File file) {
						return file.isDirectory();
					}
				});
				res.getWriter()
					.print(dirs.length);
				return;
			case "get_segment_info":
				String segmentp=req.getParameter("segment");
				int segment=Integer.valueOf(segmentp);
				return;
		}
		System.out.println(path);
		File dir = new File("./root",path);
		File[] dirs = dir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isDirectory();
			}
		});

		Arrays.sort(dirs);
		String segmentp=req.getParameter("segment");
		int segment=Integer.valueOf(segmentp);
		System.out.println(segment);
		dir=dirs[segment];

		if(!checkManifest(dir))
			genVideo(dir);
		File srcFile = new File(dir,"out.mp4");
		if(!srcFile.exists()){
			System.out.println(srcFile.getAbsolutePath());
			genVideo(dir);
		}
		res.getWriter().write(dir.getName());
		/*
		res.setContentType("video/mp4");
		res.setHeader("Content-Disposition", "filename=\""+srcFile.getName()+"\"");
		OutputStream out=
			res.getOutputStream();
		FileInputStream fis=new FileInputStream(srcFile);
		byte[] buf=new byte[1024];
		int len=fis.read(buf);
		while(len>0){
			out.write(buf,0,len);	
			len=fis.read(buf);
		}
		*/
	}

	private int getFramesCount(File dir){
		File[] files = dir.listFiles(new FilenameFilter() {
			    public boolean accept(File dir, String name) {
				            return name.toLowerCase().endsWith(".jpg");
					        }
		});
		if(files==null)
			System.out.println("files is null for: "+dir);
		return files.length;
	}

	public void genVideo(File dir)throws IOException{
		synchronized(lock){
			if(checkManifest(dir))
				return;
			System.out.println("generate video for:");
			System.out.println(dir.getAbsolutePath());
			Process p;
			int r;
			try {
				File parent=dir.getParentFile();
				System.out.println(parent.getAbsolutePath()+"/genvideo.sh");
				String[] cmd = { "sh", parent.getAbsolutePath()+"/genvideo.sh",dir.getAbsolutePath()};
				p = Runtime.getRuntime().exec(cmd); 
				p.waitFor(); 
				r=p.exitValue();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			} catch (InterruptedException e) {
				e.printStackTrace();
				return;
			}
			System.out.println("genVideo done");

			JSONObject jmanifest=new JSONObject();
			int frames=getFramesCount(dir);
			jmanifest.put("frames",frames);
			if(r!=0){
				jmanifest.put("result_code",r);

			}

			File manifest=new File(dir,"manifest.json");
			FileOutputStream fos=new FileOutputStream(manifest);
			fos.write(jmanifest.toString().getBytes());
		}
	}	

	private boolean checkManifest(File dir)throws IOException{
		JSONObject mf=readManifest(dir);
		if(mf==null)
			return false;
		if(!mf.has("frames"))
			return false;
		int framesInManifest=mf.optInt("frames",0);
		int frames=getFramesCount(dir);
		if(framesInManifest<frames)
		{
			System.out.println("addotional frames found: regenerate video...");
			return false;
		}
		return true;

	}

	private JSONObject readManifest(File dir) throws IOException{
		File manifest=new File(dir,"manifest.json");
		if(!manifest.exists())
			return null;
		FileInputStream fis=new FileInputStream(manifest);
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		byte[] buf=new byte[256];
		int len=fis.read(buf);
		while(len>0){
			baos.write(buf,0,len);
			len=fis.read(buf);
		}
		String json=new String(baos.toByteArray());
		try{
			return new JSONObject(json);
		}
		catch(Exception e){
			System.out.println("could not read json string:");

			System.out.println(json);
			return null;
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String cmd=req.getParameter("cmd");
		String path=req.getParameter("p");
		System.out.println(path);
	}
}
