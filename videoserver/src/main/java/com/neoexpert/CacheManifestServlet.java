package com.neoexpert;
import org.eclipse.jetty.servlet.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import java.util.*;
import org.json.*;

public class CacheManifestServlet extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/cache-manifest");
		PrintWriter out=
			res.getWriter();
		out.println("CACHE MANIFEST");
		out.println("CACHE:");
		out.println("/");
		out.println("/js/live_streaming.js");
	}

}
