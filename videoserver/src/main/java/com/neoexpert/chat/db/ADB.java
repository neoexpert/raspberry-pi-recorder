package com.neoexpert.chat.db;

import org.bson.Document;

public abstract class ADB{
	public User login(String id, String pw) {
		Node n=get(id);
		if(n instanceof User){
			User user=(User)n;
			if(user.login(pw))
			{
				System.out.println("user loggedin");
				return (User)user;
			}
		}
		return null;
	}
	public abstract Node get(String id);
	public abstract NodeIterator find(Document filter);
	public abstract NodeIterator find(Document filter, Document sort);
	public abstract void insert(User user, Node node);
	public abstract void addUser(User user);
	public static Node createNode(Document doc){
		switch(doc.getString("type")){
			case "user":
				return new User(doc);
			case "message":
				return new Message(doc);
			case "group":
				return new Group(doc);
		}
		return null;
	}
}
