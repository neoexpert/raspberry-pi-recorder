package com.neoexpert.chat.db;

import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.BulkWriteResult;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.Cursor;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ParallelScanOptions;
import com.mongodb.ServerAddress;
import org.bson.types.ObjectId;

import static com.mongodb.client.model.Filters.*;

import java.util.List;
import java.util.Set;
import org.bson.Document;
import com.neoexpert.chat.*;

public class MongoDB extends ADB{
	private MongoDatabase db;
	private MongoCollection<Document> nodes;

	public MongoDB(){
		MongoClient mongoClient = new MongoClient();
		db = mongoClient.getDatabase("chat");
		nodes = db.getCollection("nodes");
	}

	@Override
	public void insert(User user, Node n){
		n.sync();
		n.setOwner(user);	
		Document doc=n.doc;
		doc.put("type",n.getType());
		nodes.insertOne(doc);
		n.afterInsert();
	}

	public Node get(String id){
		Document doc;
		try{
			doc=nodes.find(eq("_id",new ObjectId(id))).first();
		}
		catch(IllegalArgumentException e){
			doc=nodes.find(eq("_id",id)).first();
		}
		if(doc==null)
			return null;
		return createNode(doc);
	}

	public NodeIterator find(Document filter){
		return null;	
	}

	public NodeIterator find(Document filter, Document sort){
		filter.put("visible",true);
		return new MongoNodeIterator(
		nodes.find(filter).sort(sort));
	}

	@Override
	public void addUser(User user){
		Document doc=user.doc;
		doc.put("type",user.getType());
		nodes.insertOne(doc);
		user.afterInsert();
		System.out.println("userID: "+user.doc.get("_id"));
	}


}
