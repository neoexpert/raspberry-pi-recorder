package com.neoexpert.chat.db;

import com.mongodb.client.FindIterable;
import org.bson.Document;
import java.util.Iterator;
import com.neoexpert.chat.*;

public class MongoNodeIterator extends NodeIterator{

	private Iterator<Document> it;
	public MongoNodeIterator(FindIterable<Document> fi){
		this.it=fi.iterator();	
	}

	public boolean hasNext(){
		return it.hasNext();
	}

	public Node next(){
		if(!it.hasNext())
			return null;
		Document doc=it.next();
		if(doc==null)
			return null;
		switch(doc.getString("type")){
			case "user":
				return new User(doc);
			case "message":
				return new Message(doc);
		}
		return null;
	}
}
