package com.neoexpert.chat.db;

import org.bson.Document;

public abstract class Node{
	protected Document doc;
	private String id;

	public Node(){
		doc=new Document();
	}
	
	public Node(Document doc){
		this.doc=doc;
		Object _id=doc.get("_id");
		if(_id!=null)
			id=_id.toString();
	}

	public final void setOwner(User user){
		doc.put("owner",user.getID());
	}

	public String getID(){
		return id;
	}

	protected final void afterInsert(){
		id=doc.get("_id").toString();
	}

	public abstract void sync();
	public abstract String getType();
	public Document getDoc(){
		return doc;
	}
	public String toString(){
		return doc.toJson();
	}
}
