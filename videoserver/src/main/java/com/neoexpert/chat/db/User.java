package com.neoexpert.chat.db;

import com.neoexpert.chat.db.Node;
import org.bson.Document;

public class User extends Node{
	private String pwhash;
	public User(Document doc){
		super(doc);
		pwhash=(String)doc.remove("pw");
	}

	public User(String pw){
		doc.put("pw", pw);
	}
	public boolean login(String pw){
		return pwhash.equals(pw);
	}


	@Override
	public void sync(){
	}

	@Override
	public String getType(){
		return "user";
	}
}
