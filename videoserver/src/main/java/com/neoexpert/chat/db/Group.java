package com.neoexpert.chat.db;

import java.util.*;
import com.neoexpert.chat.*;
import org.bson.Document;


public class Group extends Node{
	private String id;
	public Group(String id){
		this.id=id;
	}

	public Group(Document doc){
		super(doc);
	}

	private Map<String,Subscription> clients=new HashMap<>();

	public void send(Document doc){
		for(Subscription s:clients.values()){
			s.send(doc);
		}
	}

	public void addSubscription(Client client, Subscription s){
		clients.put(client.getID(),s);
		client.addGroup(this);
	}

	public void removeClient(String id){
		System.out.println("client to Remove: "+id);
		Subscription c=clients.remove(id);
		System.out.print("client removed: ");
		System.out.println(c.getClient().getID());
	}

	public boolean isEmpty(){
		return clients.isEmpty();
	}

	public int size(){
		return clients.size();
	}

	@Override
	public void sync(){
	}

	@Override
	public String getType(){
		return "message";
	}
}
