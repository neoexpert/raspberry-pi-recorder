package com.neoexpert.chat;

import com.neoexpert.chat.commands.*;
import com.neoexpert.chat.db.*;
import java.util.*;
import org.bson.Document;

public class ChatServer{
	ADB db;
	public static final int REGISTER=1;
	public static final int LOGIN=2;
	public static final int LOGIN_FAILED=3;
	public static final int LOGIN_SUCCESS=4;
	public static final int RETURN=5;
	public static final int GET=6;
	public static final int FIND=7;
	public static final int NEXT=8;
	public static final int ITERATOR_DONE=9;
	public static final int SUBSCRIBE=10;
	public static final int INSERT=11;
	public static final int UPDATE=12;
	public static final int DELETE=13;

	public ChatServer(){
		db=new MongoDB();
	}

	public ADB getDB(){
		return db;
	}

	private Map<String, Group> groups=new HashMap<>();

	public static interface BroadcastSender{
		public void sendToAll(String message);
	}

	BroadcastSender bs;
	public void setBroadcastSender(BroadcastSender bs){
		this.bs=bs;
	}

	public void onMessage(Client client, String message){
		System.out.println(message);
		Document doc = Document.parse(message);
		Document data;
		if(doc.containsKey("cmd"))
		{
			try{
				client.setCommand(process(doc,client, this));
			}
			catch(Exception e){
				e.printStackTrace(System.err);
				return;
			}
			data=(Document)doc.get("data");
			return;
		}
		else
			data=doc;
		Command command=client.getCommand();
		if(command!=null)
			command.process(data);
	}

	public void notify(String userID, Node node){
	
	}
	public void notifyGroup(String groupID, Node node){
		Message m=(Message)node;
		bs.sendToAll(m.getMessage());	
	}


	public Command process(Document doc, Client client, ChatServer chat_server){
		switch(doc.getInteger("cmd")){
			case REGISTER: client.register();
				return null;
			case LOGIN:
				User user=db.login(doc.getString("id"), doc.getString("pw"));
				if(user==null)
					client.send("{\"cmd\":3}");
				else
				{
					System.out.print("user logged in. ID: ");
					System.out.println(user.getID());
					client.setUser(user);
					client.send("{\"cmd\":4}");
				}
				return null;
			case GET:
				Node n=db.get(doc.getString("_id"));
				Document ret=new Document();
				ret.put("data",n.getDoc());
				ret.put("cmd",RETURN);
				ret.put("rid",doc.get("rid"));
				client.send(ret.toJson());
				return null;
			case FIND:
				Document filter=(Document)doc.get("filter");
				Document sort=(Document)doc.get("sort");
				client.setIterator(db.find(filter,sort),doc.getInteger("rid"));
				return null;
			case NEXT:
				client.next();
				return null;
			case SUBSCRIBE:
				String id=doc.getString("_id");
				if(id==null){
					return null;
				}
				Node node=db.get(id);
				if(node==null)
				{
					System.out.println("SUBSCRIBE: node was not found");
					return null;
				}
				Group group=groups.get(id);
				if(group==null){
					group=new Group(id);
					groups.put(id,group);
				}

				Subscription s=new Subscription(client,doc.getInteger("rid"));
				group.addSubscription(client, s);
				client.addGroup(group);
				System.out.print("subscribed to ");
				System.out.print(group);
				System.out.print(" (");
				System.out.print(group.size());
				System.out.println(" Clients)");
				return null;
			case INSERT:
				Document node_doc=(Document)doc.get("data");
				node=ADB.createNode(node_doc);	
				db.insert(client.getUser(),node);
				String parent_id=node.getDoc().get("parent").toString();
				group=groups.get(parent_id);
				if(group!=null){
					group.send(node.getDoc());
				}
				/*
				Receivable r=client.getReceivable();
				if(r!=null)
				{
					node_doc.put("owner",client.getUser().getID());
					r.send(node_doc);
				}
				*/
				return null;
		}
		throw new RuntimeException("wrong command number: "+doc.getInteger("cmd"));
	}
}
