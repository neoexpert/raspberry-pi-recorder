package com.neoexpert.chat;


import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.*;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.server.handler.*;
import java.net.URL;
import java.net.URI;
import java.io.*;
import com.neoexpert.video.*;
import com.neoexpert.*;



/**
 * Example of setting up a Jetty WebSocket server
 * <p>
 * Note: this uses the Jetty WebSocket API, not the javax.websocket API.
 */
public class WebSocketServer
{
		/**
		 * Servlet layer
		 */
		@SuppressWarnings("serial")
		public static class ChatServlet extends WebSocketServlet
		{
				@Override
				public void configure(WebSocketServletFactory factory )
				{
						// Register the echo websocket with the basic WebSocketCreator
						factory.register(ChatSocket.class);
				}
		}


    public static void main( String[] args ) throws Exception
    {
		int port=80;
		for(int i=0;i<args.length;i++){
			String arg=args[i];
			if(arg.startsWith("-")){
				switch(arg){
					case "-p":
						port=Integer.valueOf(args[i+1]);
				}
			}
		}

	    Server server = new Server(port);

		ServletContextHandler handler;
		HandlerList handlers = new HandlerList();
		server.setHandler(handlers);

		if(new File("./root").isDirectory())
		{
				System.out.println("root folder found");
			handler=initWebRoot();
		}
		else
			handler=initJarWebRoot();
	    handler.addServlet(new ServletHolder(ChatServlet.class), "/chat");
	    handler.addServlet(new ServletHolder(VideoServlet.class), "/video");
	    handler.addServlet(new ServletHolder(CacheManifestServlet.class), "/cache-manifest");
		MovedContextHandler rewriteHandler = new MovedContextHandler();
		rewriteHandler.setContextPath("/");
		rewriteHandler.setPermanent(true);
		rewriteHandler.setDiscardPathInfo(true);
		rewriteHandler.setDiscardQuery(false);
		rewriteHandler.setVirtualHosts(new String[] {"www.juwelier-asmin.de"});
		rewriteHandler.setNewContextURL("http://juwelier-asmin.de");
	    handlers.addHandler(rewriteHandler);

	    handlers.addHandler(handler);
	    server.start();
	    server.join();
    }

	public static ServletContextHandler initWebRoot()throws Exception{
	    ServletContextHandler context = new ServletContextHandler(
			ServletContextHandler.SESSIONS);
		
	    // Resolve file to directory
	    context.setContextPath("/");
		context.setResourceBase("./root");
	    context.setWelcomeFiles(new String[]{"index.html"});
	    ServletHolder holderPwd = new ServletHolder("default", DefaultServlet.class);
	    holderPwd.setInitParameter("dirAllowed","true");
	    context.addServlet(holderPwd,"/");
	    // Add the echo socket servlet to the /echo path map
	    context.dumpStdErr();
		return context;
	}

	public static ServletContextHandler initJarWebRoot()throws Exception{
	    ServletContextHandler context = new ServletContextHandler(
			ServletContextHandler.SESSIONS);
	    // Figure out what path to serve content from
	    ClassLoader cl = WebSocketServer.class.getClassLoader();
	    // We look for a file, as ClassLoader.getResource() is not
	    // designed to look for directories (we resolve the directory later)
	    URL f = cl.getResource("root/");
	    if (f == null)
	    {
		    throw new RuntimeException("Unable to find resource directory");
	    }

	    // Resolve file to directory
	    URI webRootUri = f.toURI();
	    System.err.println("WebRoot is " + f);
	    context.setContextPath("/");
	    context.setBaseResource(Resource.newResource(webRootUri));
	    context.setWelcomeFiles(new String[]{"index.html"});
	    ServletHolder holderPwd = new ServletHolder("default", DefaultServlet.class);
	    holderPwd.setInitParameter("dirAllowed","true");
	    context.addServlet(holderPwd,"/");
	    // Add the echo socket servlet to the /echo path map
	    context.dumpStdErr();
		return context;
	}
}
