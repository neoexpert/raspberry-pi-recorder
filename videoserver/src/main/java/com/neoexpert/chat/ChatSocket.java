package com.neoexpert.chat;


import java.util.*;
import java.io.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.*;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.eclipse.jetty.util.resource.Resource;
import java.net.URL;
import java.net.URI;

/**
 * Example of a Jetty API WebSocket Chat Socket
 */
@WebSocket
public class ChatSocket
{
	static ChatServer chat_server=new ChatServer();
	static HashMap<Session,Client> sessions=new HashMap<>();

	static{
		ChatServer.BroadcastSender bs=new ChatServer.BroadcastSender(){
			@Override
			public void sendToAll(String message) {
				synchronized (sessions) {
					// Iterate over the connected sessions
					// and broadcast the received message
					for (Session session : sessions.keySet()) {
						// if (!client.equals(session)){
						session.getRemote().sendStringByFuture(message);
						// }
					}
				}
			}
		};
		chat_server.setBroadcastSender(bs);
	}

	@OnWebSocketMessage
	public void onMessage(Session session, String message )
	{
		chat_server.onMessage(client,message);
		//session.getRemote().sendStringByFuture(message);
		
	}

	@OnWebSocketClose
	public void onClose(Session session, int statusCode, String reason) {
		Client client=sessions.remove(session);
		client.removeFromAllGroups();
		System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
	}

	@OnWebSocketError
	public void onError(Throwable t) {
		t.printStackTrace();
	}

	private static class WebClient extends Client{
		Session session;
		private String ID;
		private static long nextID=1;
		public WebClient(ChatServer chat_server, Session session){
			super(chat_server);
			this.session=session;	
			ID=nextID+++"";
		}
		@Override
		public void send(String message){
				try{
					session.getRemote().sendStringByFuture(message);
				}catch(Exception e){
					removeFromAllGroups();	
				}
		}

		@Override
		public String getID(){
			return ID;
		}
	}
	Client client;
	@OnWebSocketConnect
	public void onConnect(Session session) {
		client=new WebClient(chat_server, session);
		sessions.put(session,client);
		System.out.println("Connected");
	}
}
