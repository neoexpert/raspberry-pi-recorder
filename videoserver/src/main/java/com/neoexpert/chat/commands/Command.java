package com.neoexpert.chat.commands;

import com.neoexpert.chat.*;
import org.bson.Document;

public abstract class Command{
	protected ChatServer chat_server;
	protected Client client;
	public Command(Client client, ChatServer chat_server){
		this.chat_server=chat_server;
		this.client=client;
	}

	public abstract void process(Document doc);
}
