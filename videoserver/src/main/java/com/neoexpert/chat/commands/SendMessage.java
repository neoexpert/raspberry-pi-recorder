package com.neoexpert.chat.commands;

import com.neoexpert.chat.*;
import com.neoexpert.chat.db.*;
import org.bson.Document;

public class SendMessage extends Command{
	private String to;
	public SendMessage(Document doc, Client client, ChatServer chat_server){
		super(client, chat_server);
		to=doc.getString("to");
	}

	public void process(Document data){
		Message m=new Message(client.getID(), to);
		m.setMessage(data.getString("text"));
		chat_server.getDB().insert(client.getUser(),m);
		chat_server.notify(to,m);
	}
}
