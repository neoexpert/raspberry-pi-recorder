package com.neoexpert.chat.commands;

import com.neoexpert.chat.*;
import com.neoexpert.chat.db.*;
import org.bson.Document;

public class SendGroupMessage extends Command{
	private String group;
	public SendGroupMessage(Document doc, Client client, ChatServer chat_server){
		super(client, chat_server);
		group=doc.getString("group");
	}

	public void process(Document data){
		Message m=new Message(client.getID());
		m.setGroup(group);
		m.setMessage(data.getString("message"));
		chat_server.getDB().insert(client.getUser(), m);
		chat_server.notifyGroup(group,m);
	}
}
