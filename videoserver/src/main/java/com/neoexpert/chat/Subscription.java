package com.neoexpert.chat;

import org.bson.Document;
import com.neoexpert.chat.commands.Command;
import com.neoexpert.chat.db.*;
import java.util.UUID;
import java.util.*;


public class Subscription{
	private Client client;
	private int subscription_id;
	public Subscription(Client client, int subscription_id){
		this.client=client;
		this.subscription_id=subscription_id;
	}
	public Client getClient(){
		return client;
	}

	public void send(Document data){
		Document doc=new Document();
		doc.put("cmd",ChatServer.RETURN);
		doc.put("data",data);
		doc.put("rid",subscription_id);
		client.send(doc.toJson());
	}
}
