package com.neoexpert.chat;

import org.bson.Document;
import com.neoexpert.chat.commands.Command;
import com.neoexpert.chat.db.*;
import java.util.UUID;
import java.util.*;


public abstract class Client implements Receivable{
	//client_command
	private Command cmd;
	private User user;
	ChatServer chat_server;
	Set<Group> groups=new HashSet<>();
	public Client(ChatServer chat_server){
		this.chat_server=chat_server;
	}

	public abstract void send(String msg);

	private Receivable r;
	public void setReceivable(Receivable r){
		this.r=r;
	}
	public Receivable getReceivable(){
		return r;
	}

	public void setCommand(Command cmd){
		this.cmd=cmd;
	}

	public Command getCommand(){
		return cmd;
	}

	public void setUser(User user){
		this.user=user;
	}

	public User getUser(){
		return user;
	}

	public abstract String getID();

	public void register(){
		System.out.println("REGISTER");
		if(user!=null)
			throw new RuntimeException("already registered");
		UUID uuid = UUID.randomUUID();
        	String randompw = uuid.toString();
		User user=new User(randompw);
		ADB db=chat_server.getDB();
		db.addUser(user);
		Document login_info=new Document();
		login_info.put("cmd", "login_data");
		login_info.put("pw", randompw);
		login_info.put("id", user.getID());
		send(login_info.toJson());
		
	}
	private NodeIterator nit;
	private int rid;
	
	public void setIterator(NodeIterator it, int rid){
		this.nit=it;
		this.rid=rid;
	}

	public void next(){
		Document next=new Document();
		Node node=nit.next();
		if(node==null)
		{
			next.put("cmd",ChatServer.ITERATOR_DONE);
		}
		else{
			next.put("cmd",ChatServer.NEXT);
			next.put("rid",rid);
			next.put("next",node.getDoc());
		}
		send(next.toJson());
	}

	public void addGroup(Group g){
		groups.add(g);
	}

	public void removeGroup(Group g){
		groups.remove(g);
	}

	public void removeFromAllGroups(){
		for(Group g:groups)
			g.removeClient(getID());
	}
}
