package com.neoexpert.chat;

import org.bson.Document;

public interface Receivable{
	void send(String msg);
}
